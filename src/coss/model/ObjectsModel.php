<?php
declare (strict_types=1);

namespace mark\coss\model;

use mark\auth\Authorize;
use mark\coss\core\CossUtil;
use mark\coss\Coss;
use mark\http\Client\Client;
use mark\response\Response;

/**
 * Class ObjectsModel
 *
 * @package mark\coss\model
 */
final class ObjectsModel extends Coss {

    /**
     * 根据标识符获取频道信息
     *
     * @param string $bucket
     * @param string $object
     * @param array  $options
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public function info(string $bucket, string $object, $options = array(), $cache = true): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($bucket)) {
            return Response::create('', 412, '', '无效的BucketName');
        }
        if (empty($object)) {
            return Response::create('', 412, '', '无效的对象名称');
        }

        $cacheKey = 'storage:sdk:objects:info:appid:' . $this->appid . ':bucket:' . $bucket . ':object:' . $object;

        if (!empty($options['folder'])) {
            $cacheKey .= ':folder:' . ($options['folder'] ?? '');
        }
        if (!empty($options['type'])) {
            $cacheKey .= ':type:' . ($options['type'] ?? '');
        }
        if (!empty($options['suffix'])) {
            $cacheKey .= ':suffix:' . ($options['suffix'] ?? '');
        }

        return Client::getInstance()
                     ->addHeader('appid', $this->appid)
                     ->addHeader('appsecret', $this->appsecret)
                     ->appendData('bucket', $bucket)
                     ->appendData('object', $object)
                     ->append($options)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(self::$host . '/api.php/objects/info', 'json')
                     ->api_decode();
    }

    /**
     * 获取文件列表
     *
     * @param string $bucket
     * @param array  $options
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public function list(string $bucket, $options = array(), $cache = true): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($bucket)) {
            return Response::create('', 412, '', '无效的BucketName');
        }

        $cacheKey = 'storage:sdk:objects:list:appid:' . $this->appid . ':bucket:' . $bucket;

        if (!empty($options['folder'])) {
            $cacheKey .= ':folder:' . ($options['folder'] ?? '');
        }
        if (!empty($options['type'])) {
            $cacheKey .= ':type:' . ($options['type'] ?? '');
        }
        if (!empty($options['suffix'])) {
            $cacheKey .= ':suffix:' . ($options['suffix'] ?? '');
        }

        return Client::getInstance()
                     ->addHeader('appid', $this->appid)
                     ->addHeader('appsecret', $this->appsecret)
                     ->appendData('bucket', $bucket)
                     ->append($options)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('cache', $cache)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(self::$host . '/api.php/objects/list', 'json')
                     ->api_decode();
    }

    /**
     * 上传文件（Uploads a local file）
     *
     * @param string $bucket bucket name
     * @param string $object object name
     * @param string $file   local file path
     * @param array  $options
     *
     * @return \mark\response\Response
     */
    public function upload(string $bucket, $object, $file, $options = array()): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($object ?: '')) {
            return Response::create('', 412, '', '无效的Object');
        }

        // $this->precheckCommon($bucket, $object, $options);
        // CossUtil::throwOssExceptionWithMessageIfEmpty($file, 'file path is invalid');
        $file = CossUtil::encodePath($file);
        if (!file_exists($file)) {
            return Response::create('', 415, 'file does not exist', '无效的文件');
        }
        $options[self::COSS_FILE_UPLOAD] = $file;

        $options[self::COSS_BUCKET] = $bucket;
        $options[self::COSS_TITLE] = basename($object);
        $options[self::COSS_OBJECT] = $object;
        $options[self::COSS_SIZE] = sprintf('%u', filesize($options[self::COSS_FILE_UPLOAD]));
        $options[self::COSS_MD5] = md5_file($options[self::COSS_FILE_UPLOAD]);
        $options[self::COSS_HASH] = hash_file('sha256', $options[self::COSS_FILE_UPLOAD]);
        if (!isset($options[self::COSS_MIME])) {
            $options[self::COSS_MIME] = $this->getMimeType($object, $file);
        }
        $options[self::COSS_METHOD] = self::COSS_HTTP_PUT;

        // $response = $this->auth($options);
        // $result = new PutSetDeleteResult($response);
        // return $result->getData();

        $client = Client::getInstance()
                        ->appendData('appid', $this->appid)
                        ->appendData('appsecret', $this->appsecret)
                        ->appendData('endpoint', $this->endpoint)
                        ->append($options);

        if (file_exists($file)) {
            $client->appendFile($file);
        }

        return $client->addHeader('pattern', Authorize::getInstance()->getPattern())
                      ->addHeader('auth-type', Authorize::$_type)
                      ->addHeader('auth-version', Authorize::$_version)
                      ->addHeader('coss-sdk-name', self::COSS_SDK_NAME)
                      ->addHeader('coss-sdk-version', self::COSS_SDK_VERSION)
                      ->addHeader('coss-sdk-build', self::COSS_SDK_BUILD)
                      ->addHeader('coss-sdk-author', self::COSS_SDK_AUTHOR)
                      ->upload(self::$host . '/api.php/objects/upload', $options)
                      ->api_decode();
    }

    /**
     * Uploads a local file
     *
     * @param string $bucket bucket name
     * @param string $object object name
     * @param string $file
     * @param null   $options
     *
     * @uses                      OssClient::public function uploadFile($bucket, $object, $file, $options = null)
     * @deprecated                有待验证
     */
    public function putFile(string $bucket, string $object, string $file, $options = null) {
        // $this->curl->append(array('bucket' => $bucket));

        // $this->curl->push("object", $object);

        if (!file_exists(dirname($file))) {
            return Response::create('', 415, 'file does not exist', '无效的文件');
        }

        // $this->curl->appendFile($file);
        // $this->curl->appendFile($object);

        if ($options != null) {
            if (is_array($options)) {
                // $this->curl->append($options);
            }

            // $options[self::COSS_CONTENT_MD5] = md5_file(dirname($file));
            $options = array_merge(pathinfo($file), $options);

            // $this->curl->append(array(self::COSS_CALLBACK . '_' . $options[self::COSS_CONTENT_MD5] => json_encode($options, JSON_UNESCAPED_UNICODE)));
        }
    }

    /**
     * 普通文件上传
     *
     * @param      $bucket
     * @param      $object
     * @param      $file
     * @param null $options
     *
     * @return $this
     */
    public function uploadFile($bucket, $object, $file, $options = null) {
        if (file_exists(realpath($file))) {
            // $this->curl->appendFile($file);

            $finfo = new \finfo(FILEINFO_MIME_TYPE);
            $mime = $finfo->file(realpath($file));

            $HTTP_DATA = array(
                Coss::COSS_BUCKET => $bucket,
                Coss::COSS_OBJECT => $object,
                Coss::COSS_NAME => basename($object),
                Coss::COSS_MIME => $mime,
                Coss::COSS_SIZE => filesize(realpath($file)),

                Coss::COSS_MD5 => md5_file(realpath($file)),
                Coss::COSS_SHA1 => sha1_file(realpath($file))
            );
            if ($options != null) {
                $HTTP_DATA = array_merge($HTTP_DATA, $options);
            }

            $HTTP_DATA = array_merge(pathinfo($file), $HTTP_DATA);

            // $this->curl->appendData(md5_file(realpath($file)), json_encode($HTTP_DATA));
        }

        return $this;
    }

    /**
     * 更新文件
     *
     * @param string $object
     * @param array  $options
     *
     * @return \mark\response\Response
     */
    public function update(string $object, $options = array()): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($object ?: '')) {
            return Response::create('', 412, '', '无效的Object');
        }

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->appendData('endpoint', $this->endpoint)
                     ->append($options)
                     ->appendData('object', $object)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(self::$host . '/api.php/objects/update', 'json')
                     ->api_decode();
    }

    /**
     * 移除文件
     *
     * @param string $object
     * @param array  $options
     *
     * @return \mark\response\Response
     */
    public function remove(string $object, $options = array()): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($object ?: '')) {
            return Response::create('', 412, '', '无效的Object');
        }

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->appendData('endpoint', $this->endpoint)
                     ->append($options)
                     ->appendData('object', $object)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(self::$host . '/api.php/objects/remove', 'json')
                     ->api_decode();
    }

    /**
     * 删除文件
     *
     * @param string $object
     * @param array  $options
     *
     * @return \mark\response\Response
     */
    public function delete(string $object, $options = array()): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($object ?: '')) {
            return Response::create('', 412, '', '无效的Object');
        }

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->appendData('endpoint', $this->endpoint)
                     ->append($options)
                     ->appendData('object', $object)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(self::$host . '/api.php/objects/delete', 'json')
                     ->api_decode();
    }

}