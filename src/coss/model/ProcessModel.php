<?php
declare (strict_types=1);

namespace mark\coss\model;

use mark\auth\Authorize;
use mark\coss\Coss;
use mark\http\Client\Client;
use mark\response\Response;

/**
 * Class ProcessModel
 *
 * @package mark\coss\model
 */
final class ProcessModel extends Coss {

    /**
     * 获取规则详情
     *
     * @param string $bucket
     * @param string $process
     * @param array  $options
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public function info(string $bucket, string $process, $options = array(), $cache = true): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($bucket)) {
            return Response::create('', 412, '', '无效的BucketName');
        }
        if (empty($process)) {
            return Response::create('', 412, '', '无效的Process');
        }
        $cacheKey = 'storage:sdk:process:info:appid:' . $this->appid . ':bucket:' . $bucket . ':process:' . $process;

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->appendData('bucket', $bucket)
                     ->appendData('process', $process)
                     ->append($options)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(self::$host . '/api.php/process/info', 'json')
                     ->api_decode();
    }

    /**
     * 获取规则列表
     *
     * @param string $bucket
     * @param array  $options
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public function list(string $bucket, array $options = array(), $cache = true): Response {
        if (empty($bucket)) {
            return Response::create('', 412, '', '无效的BucketName');
        }

        $cacheKey = 'storage:sdk:process:list:appid:' . $this->appid . ':bucket:' . ($bucket ?: '');

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->appendData('bucket', $bucket ?: '')
                     ->append($options)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(self::$host . '/api.php/process/list', 'json')
                     ->api_decode();
    }

    /**
     * 创建规则
     *
     * @param string $process
     * @param array  $options
     *
     * @return \mark\response\Response
     */
    public function create(string $process, $options = array()): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($process ?: '')) {
            return Response::create('', 412, '', '无效的Process');
        }

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->append($options)
                     ->appendData('process', $process)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(self::$host . '/api.php/process/create', 'json')
                     ->api_decode();
    }

    /**
     * 更新规则
     *
     * @param string $process
     * @param array  $options
     *
     * @return \mark\response\Response
     */
    public function update(string $process, $options = array()): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($process ?: '')) {
            return Response::create('', 412, '', '无效的Process');
        }

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->append($options)
                     ->appendData('process', $process)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(self::$host . '/api.php/process/update', 'json')
                     ->api_decode();
    }

    /**
     * 删除规则
     *
     * @param string $process
     * @param array  $options
     *
     * @return \mark\response\Response
     */
    public function delete(string $process, $options = array()): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($process ?: '')) {
            return Response::create('', 412, '', '无效的Process');
        }

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->append($options)
                     ->appendData('process', $process)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(self::$host . '/api.php/process/delete', 'json')
                     ->api_decode();
    }
}