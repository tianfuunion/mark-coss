# mark-coss

#### 介绍

{**基于MarkEngine实现的对象存储系统**
，请看 [https://coss.tianfu.ink](https://coss.tianfu.ink)}

## 安装方法

如果您通过composer管理您的项目依赖，可以在你的项目根目录运行：

        $ composer require tianfuunion/mark-coss

或者在你的`composer.json`中声明对 Mark Auth SDK For PHP 的依赖：

        "require": {
            "tianfuunion/mark-coss": "^2.0"
        }

然后通过`composer install`安装依赖。composer安装完成后，在您的PHP代码中引入依赖即可：

        require_once __DIR__ . '/vendor/autoload.php';

## 使用说明

### License

- MulanPSL-2.0

### 联系我们

- [天府联盟官方网站：www.tianfuunion.cn](https://www.tianfuunion.cn)
- [天府授权中心官方网站：auth.tianfu.ink](https://auth.tianfu.ink)
- [天府联盟反馈邮箱：report@tianfu.ink](mailto:report@tianfu.ink)