<?php
declare (strict_types=1);

namespace mark\coss\model;

use mark\auth\Authorize;
use mark\coss\Coss;
use mark\http\Client\Client;
use mark\response\Response;

/**
 * Class BucketModel
 *
 * @package mark\coss\model
 */
final class BucketModel extends Coss {

    /**
     * 获取Bucket信息
     *
     * @param string $bucket bucket name
     * @param array  $options
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public function info(string $bucket, $options = array(), $cache = true): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($bucket ?: '')) {
            return Response::create('', 412, '', '无效的BucketName');
        }

        $options[self::COSS_BUCKET] = $bucket;
        $options[self::COSS_METHOD] = self::COSS_HTTP_GET;
        $options[self::COSS_OBJECT] = '/';
        $options[self::COSS_SUB_RESOURCE] = 'bucketInfo';

        $cacheKey = 'storage:sdk:bucket:info:appid:' . $this->appid . ':bucket:' . $bucket;

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->append($options)
                     ->appendData('bucket', $bucket)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->addHeader('coss-sdk-name', self::COSS_SDK_NAME)
                     ->addHeader('coss-sdk-version', self::COSS_SDK_VERSION)
                     ->addHeader('coss-sdk-build', self::COSS_SDK_BUILD)
                     ->addHeader('coss-sdk-author', self::COSS_SDK_AUTHOR)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(self::$host . '/api.php/bucket/info', 'json')
                     ->api_decode();
    }

    /**
     * 获取Bucket列表
     *
     * @param array $options
     * @param bool  $cache
     *
     * @return \mark\response\Response
     */
    public function list($options = array(), $cache = true): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }

        $cacheKey = 'storage:sdk:bucket:list:appid:' . $this->appid;

        return Client::getInstance()
                     ->addHeader('appid', $this->appid)
                     ->addHeader('appsecret', $this->appsecret)
                     ->append($options)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(self::$host . '/api.php/bucket/list', 'json')
                     ->api_decode();
    }

    /**
     * 创建Bucket
     *
     * @param string $bucket
     * @param array  $options
     *
     * @return \mark\response\Response
     */
    public function create(string $bucket, $options = array()): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($bucket ?: '')) {
            return Response::create('', 412, '', '无效的BucketName');
        }

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->append($options)
                     ->appendData('bucket', $bucket)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(self::$host . '/api.php/bucket/create', 'json')
                     ->api_decode();
    }

    /**
     * 更新Bucket
     *
     * @param string $bucket
     * @param array  $options
     *
     * @return \mark\response\Response
     */
    public function update(string $bucket, $options = array()): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($bucket ?: '')) {
            return Response::create('', 412, '', '无效的BucketName');
        }

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->append($options)
                     ->appendData('bucket', $bucket)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(self::$host . '/api.php/bucket/update', 'json')
                     ->api_decode();
    }

    /**
     * 删除Bucket
     *
     * @param string $bucket
     * @param array  $options
     *
     * @return \mark\response\Response
     */
    public function delete(string $bucket, $options = array()): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($bucket ?: '')) {
            return Response::create('', 412, '', '无效的BucketName');
        }

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->append($options)
                     ->appendData('bucket', $bucket)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->post(self::$host . '/api.php/bucket/delete', 'json')
                     ->api_decode();
    }
}