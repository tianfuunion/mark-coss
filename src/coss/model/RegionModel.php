<?php
declare (strict_types=1);

namespace mark\coss\model;

use mark\auth\Authorize;
use mark\coss\Coss;
use mark\http\Client\Client;
use mark\response\Response;

/**
 * Class RegionModel
 *
 * @package mark\coss\model
 */
final class RegionModel extends Coss {

    /**
     * 获取区域详情
     *
     * @param string $region
     * @param array  $options
     * @param bool   $cache
     *
     * @return \mark\response\Response
     */
    public function info(string $region, $options = array(), $cache = true): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '缺少AppID参数');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }
        if (empty($region)) {
            return Response::create('', 412, '', '无效的Region');
        }

        $cacheKey = 'storage:sdk:region:info:region:' . $region;

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->appendData('region', $region)
                     ->append($options)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(self::$host . '/api.php/region/info', 'json')
                     ->api_decode();
    }

    /**
     * 获取区域列表
     *
     * @param array $options
     * @param bool  $cache
     *
     * @return \mark\response\Response
     */
    public function list($options = array(), $cache = true): Response {
        if (empty($this->appid)) {
            return Response::create('', 412, '', '无效的AppID');
        }
        if (empty($this->appsecret)) {
            return Response::create('', 412, '', '无效的AppSecret');
        }

        $cacheKey = 'storage:sdk:region:list';

        return Client::getInstance()
                     ->appendData('appid', $this->appid)
                     ->appendData('appsecret', $this->appsecret)
                     ->append($options)
                     ->addHeader('pattern', Authorize::getInstance()->getPattern())
                     ->addHeader('auth-type', Authorize::$_type)
                     ->addHeader('auth-version', Authorize::$_version)
                     ->setExpire(Authorize::getInstance()->getExpire())
                     ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                     ->setCacheKey($cacheKey)
                     ->setCache($cache)
                     ->setCallback(function (Response $response, Client $client) {
                         if ($response->api_decode()->getResponseCode() != 200) {
                             $client->setCache(false);
                             $client->clearCache();
                         }

                         return $response;
                     })
                     ->get(self::$host . '/api.php/region/list', 'json')
                     ->api_decode();
    }

}